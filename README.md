# Ground-truth-collection

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
- WebSocketServerProtocol
- demjson
- asyncio
```

### Installing

A step by step series of examples that tell you how to get a development env running

##### Step 1: Setup PUX camera

```
- Access PUX camera adress.
- Setting serverName change to your PC IP in Aplications/FaceAttrRecgG2 (Remember port)
- Stop and restart FaceAttrRecgG2 module 
```

##### Step 2: Config setting in data cleaning file

```
- change SOCKET_HOST and SOCKET_PORT map to PUX setting in step 1
```
##### Step 3: Running code
```
python3 data_clearning.py
```

## Deployment

Add additional notes about how to deploy this on a live system


## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/hieulike/ground-truth-collection) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **HieuLike** - *Initial work* - [HieuLike](https://gitlab.com/hieulike)

## Acknowledgments


from datetime import datetime
import glob
import os.path
import numpy as np
import cv2
import json
import os

file_name = './data.txt'
data_frame = [[]]
data_time_stamp = []
data_age = []
data_gender = []
def json_reader():
    with open(file_name) as json_file:  
        data = json.load(json_file)
        for item in data:
            if 'frames' not in item:
                continue
            for p in item['frames']:
                time_stamp = p['timestamp']
                print('timestamp', time_stamp)
                dt_obj = datetime.strptime(time_stamp,
                            '%Y%m%d%H%M%S%f')
                millisec = dt_obj.timestamp()*1000
                print(' ---   miliseconds: %f' % (millisec))
                data_time_stamp.append(millisec)
                detail = p['details']
                for i in detail:
                    frame = i['bb']
                    data_frame.append(frame)
                    print('FRAME', frame)
                    for j in i['info']:
                        if j['tag'] == 'AGE':
                            age = j['val']
                            print('AGE', age)
                            data_age.append(age)
                        if j['tag'] == 'GENDER':
                            gender = j['val']
                            print('GENDER', gender)
                            data_gender.append(gender)
    return(data_time_stamp,data_frame, data_age, data_gender)

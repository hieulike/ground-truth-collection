import tensorflow as tf

from nets.mobilenet import mobilenet_v2
from tensorflow.contrib import slim


def endpoints(image, is_training):
    if image.get_shape().ndims != 4:
        raise ValueError('Input must be of size [batch, height, width, 3]')

    with tf.contrib.slim.arg_scope(
            mobilenet_v2.training_scope(bn_decay=0.9, weight_decay=0.0)):
        _, endpoints = mobilenet_v2.mobilenet(
            image, num_classes=1001, is_training=is_training, depth_multiplier=1.4)

    endpoints['reduce_dims'] = tf.squeeze(
        endpoints['global_pool'], [1, 2], name='reduce_dims')

    return endpoints, 'MobilenetV2'

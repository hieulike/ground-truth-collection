
from datetime import datetime
import glob
import os.path
import numpy as np
import cv2
import json
import os
import json_reader
from PIL import Image
videofps = 10
images_path ='./images'
fullimagepath ='./full_images'

def get_videos(follder_path, times):
    valid_video = ['.mp4', '.MP4']
    for f in os.listdir(follder_path):
        # count += 1
        # # process("Copying...", ratio=count/total)
        urlFile = os.path.join(follder_path,f)
        ext = os.path.splitext(f)[1]
        a = my_split(f,["."])
        name_file = a[0]
        if ext.lower() not in valid_video:
            continue
        print(name_file)
        video2image(video_path= urlFile, time_start = name_file,times = times  )
    return images_path

def filter_time(item, time):
    if item - time <= 1000:
        return True
    else:
        return False

def video2image(video_path, time_start, times):
    cap = cv2.VideoCapture(video_path)

    i = 0
    name = 0
    while cap.isOpened():
        ret, frame = cap.read()
        time = cap.get(cv2.CAP_PROP_POS_MSEC)
        index = cap.get(cv2.CAP_PROP_POS_FRAMES)
        dt_obj = datetime.strptime(time_start,
                           '%Y%m%d%H%M%S')
        millisec = dt_obj.timestamp()*1000 + time

        # print('Start date',dt_obj)
        timesss = list(filter(lambda num: num -1000 - millisec <= 1000 and num -1000- millisec >= 0, times[0]))
        if len(timesss) > 0 :
            print('MAPSSSS', timesss)
        if frame is None:
            break
        i += 1
        if videofps <= 0:
            cv2.imwrite(os.path.join(images_path, str(name)) + '.jpg', frame)
            name += 1
            print('(height: %d, weight: %d, channel: %d)' % frame.shape)
        else:
            if i == videofps:
                for t in timesss:
                    index = timesss.index(t)
                    frames = times[1]
                    _frame = frames[index+1]

                    rect_img = frame[_frame[1] : _frame[3], _frame[0] : _frame[2]]
                    face_img = rect_img[:,:,::-1]
                    img = Image.fromarray(face_img)
                    i = 0
                    date = datetime.fromtimestamp(millisec/1000.0)
                    date_str = date.strftime('%Y-%m-%d %H:%M:%S')
                    cv2.imwrite(os.path.join(images_path, str(date_str)) + '.jpg', rect_img)
                    cv2.imwrite(os.path.join(fullimagepath, str(date_str)) + '.jpg', frame)
                    name += 1
                    print(name)
                    print('(height: %d, weight: %d, channel: %d)' % frame.shape)

    cap.release()
    cv2.destroyAllWindows()
def my_split(s, seps):
    res = [s]
    for sep in seps:
        s, res = res, []
        for seq in s:
            res += seq.split(sep)
    return res
        
# if __name__ == '__main__':
#     results = json_reader.json_reader()
#     get_videos(follder_path=video_path,times = results)
# # 1. Import libs and load tf model

import tensorflow as tf
import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.preprocessing import normalize
import os
import sys
import numpy
import ntpath
import argparse
import skimage.io
import skimage.transform
import mvnc.mvncapi as mvnc


graph_path = './graphs/age_gender_graph'

def open_ncs_device():

    # Look for enumerated NCS device(s); quit program if none found.
    devices = mvnc.enumerate_devices()
    if len( devices ) == 0:
        print( "No devices found" )
        quit()

    # Get a handle to the first enumerated device and open it
    device = mvnc.Device( devices[0] )
    device.open()

    return device

def load_graph(device):

    # Read the graph file into a buffer
    with open( graph_path, mode='rb' ) as f:
        blob = f.read()

    # Load the graph buffer into the NCS
    graph = mvnc.Graph(graph_path)
    # Set up fifos
    fifo_in, fifo_out = graph.allocate_with_fifos(device, blob)

    return graph, fifo_in, fifo_out

def pre_process_image(image):

    # Read & resize image [Image size is defined during training]
    img = cv2.resize(image, (224,224))
    img = (img - 127.5) / 128
    img = np.expand_dims(img, axis=0)

    return img

def infer_image(graph, img, fifo_in, fifo_out):

    # The first inference takes an additional ~20ms due to memory 
    # initializations, so we make a 'dummy forward pass'.
    graph.queue_inference_with_fifo_elem(fifo_in, fifo_out, img.astype(numpy.float32), None)

    output, userobj = fifo_out.read_elem()
    
    # Load the image as an array
    graph.queue_inference_with_fifo_elem(fifo_in, fifo_out, img.astype(numpy.float32), None)

    # Get the results from NCS
    output, userobj = fifo_out.read_elem()

    # Get execution time
    inference_time = graph.get_option(mvnc.GraphOption.RO_TIME_TAKEN)

    # Print the results
    print("Execution time: " + str(numpy.sum( inference_time )) + "ms")
    #output = np.expand_dims(output,axis=0)
    return output

def clean_up(device, graph, fifo_in, fifo_out):
    fifo_in.destroy()
    fifo_out.destroy()
    graph.destroy()
    device.close()
    device.destroy()

import numpy as np
def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()



def map_to_gender(gender_emb):
    if np.argmax(gender_emb) == 0:
        return 'Male'
    else:
        return 'Female'
    
def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()


def predict(image, graph, fifo_in, fifo_out):

    img = cv2.imread(image)[:,:,::-1]

    img_emb = infer_image(graph, pre_process_image(img), fifo_in, fifo_out)

    age = np.argmax(softmax(img_emb[0:100])) + 1
    gender = map_to_gender(softmax(img_emb[101:]))
    print('Age: ' + str(age) + ', Gender: ' + str(gender))

    return {'time_stamp': 1, 'age': age, 'gender': gender }

# def clear():
#     clean_up(device, graph, fifo_in, fifo_out)
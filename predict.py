
import tensorflow as tf
import numpy as np
import cv2
import nets.mobilenet_v2_14_224 as model
import heads.fc as head

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.preprocessing import normalize
import os
import video_processing
import json
# Tensorflow human re-ID feature descriptor model
tf.Graph().as_default()
sess = tf.Session()
images = tf.zeros([1, 224, 224, 3], dtype=tf.float32)
endpoints, body_prefix = model.endpoints(images, is_training=False)
with tf.name_scope('head'):
    endpoints = head.head(endpoints, is_training=False)
tf.train.Saver().restore(sess, './checkpoint/checkpoint-390001')


def face_embedding(img):
    resize_img = cv2.resize(img, (224,224))
    resize_img = (resize_img - 127.5) / 128
    resize_img = np.expand_dims(resize_img, axis=0)
    ages = sess.run(endpoints['age_gender'], feed_dict={images: resize_img})
    return ages

def map_to_gender(gender_emb):
    if np.argmax(gender_emb) == 0:
        return 'Male'
    else:
        return 'Female'
    
def map_to_age_range(age_emb):
    if np.argmax(age_emb) == 0:
        return '0-9'
    elif np.argmax(age_emb) == 1:
        return '10-19'
    elif np.argmax(age_emb) == 2:
        return '20-29'
    elif np.argmax(age_emb) == 3:
        return '30-39'
    elif np.argmax(age_emb) == 4:
        return '40-49'
    elif np.argmax(age_emb) == 5:
        return '50-59'
    elif np.argmax(age_emb) == 6:
        return '60-69'
    elif np.argmax(age_emb) == 7:
        return '70-79'
    elif np.argmax(age_emb) == 8:
        return '80-120'
    
def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()
def predict(image_folder):
    imgs = []
    valid_images = [".jpg", ".png", ".jpeg", ".PNG", ".JPG", ".JPEG"]
    for img in os.listdir(image_folder):
        ext = os.path.splitext(img)[1]
        if ext.lower() not in valid_images:
            continue
        a = video_processing.my_split(img,["_", "."])
        age = a[0]
        img11_1 = cv2.imread(img)[:,:,::-1]
        result = face_embedding(img11_1)

        age = softmax(result[:,0:100])

        gender = softmax(result[:,100:])

        plt.imshow(img11_1)
        plt.title('Age: ' + str(map_to_gender(gender)) + ', Gender: ' + str(np.argmax(age)))



    


#!/usr/bin/env python3

from os import getenv

import demjson

import asyncio
from autobahn.asyncio.websocket import WebSocketServerProtocol, \
    WebSocketServerFactory

import cv2
import numpy as np
import json

SOCKET_HOST = getenv('SOCKET_HOST', '0.0.0.0')
SOCKET_PORT = getenv('SOCKET_PORT', 7429)

arrs = []
DATA_FILENAME = './data.txt'
def pux_to_json(bydata):
    try:
        data = bydata.decode('utf8').strip().replace("'", '"')
        print(data)
        return demjson.decode(data)
    except Exception as err:
        print(err)
        return None


def on_pux_data(data):
    try:
        jdata = pux_to_json(data)
       
        if jdata:
            arrs.append(jdata)
            with open(DATA_FILENAME, mode='w', encoding='utf-8') as feedsjson:
                arrs.append(jdata)
                json.dump(arrs, feedsjson)
            # print(jdata)
    except Exception as err:
        print(err)
def write_file():
    with open('log.txt', 'w') as f:
        for item in arrs:
            f.write("%s\n" % item)


class ServerProtocolPUX(WebSocketServerProtocol):
    async def onConnect(self, request):
        print('PUX socket client connected: {}'.format(request.peer))

    async def onOpen(self):
        print('PUX socket connection opened')

    async def onClose(self, wasClean, code, reason):
        print('PUX socket connection closed: {}'.format(reason))

    async def onMessage(self, payload, isbinary):
        print(payload)
        on_pux_data(payload)

class VideoStreamProducer:
    def __init__(self,proto):
        self.proto = proto
        self.started = False
        self.paused = False

    def pauseProducing(self):
        self.paused = True

    def resumeProducing(self):
        self.paused = False
    def stopProducing(self):
        self.cap.release()
        
def startServer():
    # loop = asyncio.new_event_loop()
    # asyncio.set_event_loop(loop)
    loop = asyncio.get_event_loop()

    factory = WebSocketServerFactory()
    factory.protocol = ServerProtocolPUX
    factory.setProtocolOptions(autoPingInterval=1)
    factory.setProtocolOptions(requireMaskedClientFrames=False)

    coro = loop.create_server(factory, SOCKET_HOST, SOCKET_PORT)
    server = loop.run_until_complete(coro)
    print('PUX socket server started listening at {}'.format(
        SOCKET_PORT
    ))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        print('KeyboardInterrupt')
        pass
    except Exception as err:
        print(err)
    finally:
        server.close()
        loop.close()
        print('PUX socket server closed')
             
if __name__ == '__main__':
    startServer()

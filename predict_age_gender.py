from mvnc import mvncapi as mvnc
import numpy as np
import cv2
import argparse
import time
from matplotlib import pyplot as plt

import tensorflow as tf
import numpy as np
import matplotlib.image as mpimg
from sklearn.preprocessing import normalize
import os
import sys
import numpy
import ntpath
import argparse
import skimage.io
import skimage.transform
# from PIL import ImageFont, ImageDraw, Image


def preprocess(src, INPUT_DIM):
    img = cv2.resize(src, (INPUT_DIM, INPUT_DIM))
    img = img - 127.5
    img = img * 0.007843
    return img


def postprocess_ncs(img, out):
    h = img.shape[0]
    w = img.shape[1]
    size = len(out)
    array_size = int(size / 7)
    boxes = np.reshape(out.astype(np.float32), [array_size, 7])
    num_valid_objs = int(boxes[0][0])
    return (num_valid_objs, boxes)


def detect(CLASSES, graph, fifoIn, fifoOut, IMG_FILE, INPUT_DIM, SCORE_THRESHOLD, age_gender_Graph, age_gender_fifoIn, age_gender_fifoOut):
    orig_img = IMG_FILE[:, :, ::-1].copy()
#     orig_img = cv2.imread(IMG_FILE)[:, :, ::-1].copy()
    img = preprocess(orig_img, INPUT_DIM)
    img = img.astype(np.float32)

    # Send the image to the ncs for processing
    graph.queue_inference_with_fifo_elem(fifoIn, fifoOut, img, 'user object')
    # Retrieve results
    output, userobj = fifoOut.read_elem()

    num_valid_objs, boxes = postprocess_ncs(orig_img, output)

    image_h = orig_img.shape[0]
    image_w = orig_img.shape[1]
    count = 0

    # Cycle through each detected object and set up the text and bounding boxes for each
    for obj in range(num_valid_objs):
        obj_num = obj + 1
        box = boxes[obj_num]
        class_id = box[1]
        score = box[2]
        if score >= SCORE_THRESHOLD:
            if np.isnan(box).any() or np.isinf(box).any():
                continue
            xmin = max(int(box[3] * image_w), 0)
            ymin = max(int(box[4] * image_h), 0)
            xmax = max(int(box[5] * image_w), 0)
            ymax = max(int(box[6] * image_h), 0)

            box_top_left = (xmin, ymin)
            box_bot_right = (xmax, ymax)
            color = (0, 255, 0)
            font = cv2.FONT_ITALIC
            bottomLeftCornerOfText = (xmin, ymin)
            fontScale = 1  # (image_h*image_w)/(1024*1024)
            fontColor = (255, 0, 0)
            lineType = 2
            if class_id == 2:
                count += 1
                color = (0, 0, 255)
                rect_img = orig_img[box_top_left[1]: box_bot_right[1],
                                    box_top_left[0]: box_bot_right[0]]
                face_img = rect_img[:, :, ::-1]
                img_emb = infer_image(age_gender_Graph, pre_process_image(
                    face_img), age_gender_fifoIn, age_gender_fifoOut)
                age = np.argmax(softmax(img_emb[0:101]))
                gender = map_to_gender(softmax(img_emb[101:]))
                print('Age: ' + str(age) + ', Gender: ' + str(gender))
                cv2.putText(orig_img, str(gender) + ' ' + str(age),
                            bottomLeftCornerOfText, font, fontScale, fontColor, lineType)
            cv2.rectangle(orig_img, box_top_left, box_bot_right, color)
            p3 = (max(box_top_left[0], 15), max(box_top_left[1], 15))
            obj_class = CLASSES[int(class_id)][:len(
                CLASSES[int(class_id)]) - 1]
            title = obj_class + " " + str(round(score, 2))

    return orig_img


def pre_process_image(image):

    # Read & resize image [Image size is defined during training]
    img = cv2.resize(image, (224, 224))
    img = (img - 127.5) / 128
    img = np.expand_dims(img, axis=0)

    return img


def infer_image(graph, img, fifo_in, fifo_out):

    # The first inference takes an additional ~20ms due to memory
    # initializations, so we make a 'dummy forward pass'.
    graph.queue_inference_with_fifo_elem(
        fifo_in, fifo_out, img.astype(numpy.float32), None)

    output, userobj = fifo_out.read_elem()

    # Load the image as an array
    graph.queue_inference_with_fifo_elem(
        fifo_in, fifo_out, img.astype(numpy.float32), None)

    # Get the results from NCS
    output, userobj = fifo_out.read_elem()

    # Get execution time
    inference_time = graph.get_option(mvnc.GraphOption.RO_TIME_TAKEN)

    # Print the results
    print("Execution time: " + str(numpy.sum(inference_time)) + "ms")
    #output = np.expand_dims(output,axis=0)
    return output


def clean_up(device, graph, fifo_in, fifo_out):
    fifo_in.destroy()
    fifo_out.destroy()
    graph.destroy()
    device.close()
    device.destroy()


def load_graph(device, graph_path):

    # Read the graph file into a buffer
    with open(graph_path, mode='rb') as f:
        blob = f.read()

    # Load the graph buffer into the NCS
    graph = mvnc.Graph(graph_path)
    # Set up fifos
    fifo_in, fifo_out = graph.allocate_with_fifos(device, blob)

    return graph, fifo_in, fifo_out


def open_ncs_device():

    # Look for enumerated NCS device(s); quit program if none found.
    devices = mvnc.enumerate_devices()
    if len(devices) == 0:
        print("No devices found")
        quit()

    # Get a handle to the first enumerated device and open it
    device = mvnc.Device(devices[0])
    device.open()

    return device


def map_to_gender(gender_emb):
    if np.argmax(gender_emb) == 0:
        return 'Male'
    else:
        return 'Female'


def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()


def start_camera():
    print('Strart camera')
    cap = cv2.VideoCapture(0)
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()
        # Our operations on the frame come here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # Display the resulting frame
        image = frame.array
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


BLOB_FILE = 'ssd_mob_v2_graph'
age_gender_graph_file = 'age_gender_graph'
INPUT_SIZE = 300
SCORE_THRESHOLD = 0.6

CLASSES = ['background', 'person', 'face']

# Configure the NCS log level
mvnc.global_set_option(mvnc.GlobalOption.RW_LOG_LEVEL, 2)

# Get a list of ALL the sticks that are plugged in
devices = mvnc.enumerate_devices()
if len(devices) == 0:
    print('No NCS devices found')
    quit()

# Pick the first stick to run the network
device = mvnc.Device(devices[0])

# Open the NCS
device.open()
# Create graph object by reading in the Movidius graph file
with open(BLOB_FILE, mode='rb') as f:
    graphfile = f.read()
face_graph = mvnc.Graph(BLOB_FILE)

# Allocate the graph
face_fifoIn, face_fifoOut = face_graph.allocate_with_fifos(device, graphfile)
age_graph, fifo_in_age, fifo_out_age = load_graph(
    device, graph_path=age_gender_graph_file)

print('Camera is starting...')
cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    if ret:
        img = detect(CLASSES, face_graph, face_fifoIn, face_fifoOut, frame, INPUT_SIZE, SCORE_THRESHOLD,
                     age_gender_Graph=age_graph, age_gender_fifoIn=fifo_in_age, age_gender_fifoOut=fifo_out_age)
        cv2.imshow('frame', img[:, :, ::-1])
        if cv2.waitKey(1) & 0xFF == ord('q'):
            print('Stop by user request!')
            break
        # When everything done, release the capture
    else:
        print('Camera connection fail!')
        break
cap.release()
cv2.destroyAllWindows()

# Clean up
face_fifoIn.destroy()
face_fifoOut.destroy()
face_graph.destroy()
fifo_out_age.destroy()
fifo_in_age.destroy()
age_graph.destroy()
device.close()
print('Finished')

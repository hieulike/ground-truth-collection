import age_gender_predict
import video_processing
import json_reader
import os
data = []
if __name__ == '__main__':
    # define data 
    video_path = './videos'

    results = json_reader.json_reader()
    imagespath = video_processing.get_videos(follder_path=video_path,times = results)
    device = age_gender_predict.open_ncs_device()
    graph, fifo_in, fifo_out = age_gender_predict.load_graph(device)
    valid_images = [".jpg", ".png", ".jpeg", ".PNG", ".JPG", ".JPEG"]
    for image in os.listdir(imagespath):
        ext = os.path.splitext(image)[1]
        if ext.lower() not in valid_images:
            continue
        result = age_gender_predict.predict(image=imagespath+ '/' + image,graph=graph, fifo_in= fifo_in, fifo_out=fifo_out)
        data.append(result)
    age_gender_predict.clean_up(device, graph, fifo_in, fifo_out)
    print(data)